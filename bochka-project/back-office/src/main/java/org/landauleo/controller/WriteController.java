package org.landauleo.controller;

import landauleo.CommonClient;
import landauleo.repository.MyRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WriteController {

    private final MyRepository myRepository;

    public WriteController(MyRepository myRepository) {
        this.myRepository = myRepository;
    }

    @PostMapping("/write")
    public void write(@RequestParam String text) {
        CommonClient commonClient = new CommonClient();
        commonClient.setName(text);
        myRepository.save(commonClient);
    }

    @GetMapping("/status")
    public String getStatus() {
        return "UP";
    }

    @GetMapping("/detector")
    public String gatDetector() {
        return "You are gay";
    }

    @GetMapping("/surprise")
    public String getSurprise() {
        return "You look like nerd";
    }

}
