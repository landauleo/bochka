package org.landauleo;

import landauleo.CommonClient;
import landauleo.repository.MyRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Hello world!
 *
 */

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = MyRepository.class)
@EntityScan(basePackageClasses = CommonClient.class)
public class BackOfficeApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(BackOfficeApp.class, args);
    }
}
