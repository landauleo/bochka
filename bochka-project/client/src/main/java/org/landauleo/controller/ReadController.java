package org.landauleo.controller;


import java.util.List;

import landauleo.CommonClient;
import landauleo.repository.MyRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReadController {

    private final MyRepository myRepository;

    public ReadController(MyRepository myRepository) {
        this.myRepository = myRepository;
    }

    @GetMapping("/read")
    public List<CommonClient> read() {
        return myRepository.findAll();
    }

    @GetMapping("/status")
    public String getStatus() {
        return "UP";
    }

    @GetMapping("/detector")
    public String getDetector() {
        return "You are NOT gay";
    }

    @GetMapping("/surprise")
    public String getSurprise() {
        return "You look pretty";
    }

}
