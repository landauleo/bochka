package org.landauleo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WriteController {

    @GetMapping("/hello")
    public String greeting() {
        return "Hi, dummy!";
    }
}
