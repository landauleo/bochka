package landauleo.config;

import landauleo.repository.MyRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;

//@Configuration
//@EntityScan(basePackages = "landauleo")
//@EnableJpaRepositories(basePackages = "landauleo.repository")
public class JpaConfiguration {


    @Bean
    public JpaRepositoryFactoryBean myRepo() {
        JpaRepositoryFactoryBean jpa = new JpaRepositoryFactoryBean<>(MyRepository.class);
        return jpa;
    }

}
